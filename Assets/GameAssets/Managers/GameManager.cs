﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAPI;

public class GameManager : MonoBehaviour
{
	public static BuffList buffList;
	public BuffList _buffList;

	public virtual void Awake(){
		InitializeBuffs();
	}

	public virtual void InitializeBuffs(){
		buffList = _buffList;
		for(int i = 0; i < buffList.Count; i++){
			buffList[i].typeId = i;
		}
	}

    public virtual void OnApplicationQuit() {
    	if(NetworkingManager.Singleton.IsHost) 
    		NetworkingManager.Singleton.StopHost();
    	else if (NetworkingManager.Singleton.IsServer) 
    		NetworkingManager.Singleton.StopServer();
    	else if(NetworkingManager.Singleton.IsClient) 
    		NetworkingManager.Singleton.StopClient();
    }
}
