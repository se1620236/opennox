using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleDestroyer : MonoBehaviour
{
    private ParticleSystem ps;
    public float destroyTime;

    public void Init(){
        this.enabled = true;
        ps = GetComponent<ParticleSystem>();
        if(ps){
            ParticleSystem.MainModule main;
            ps.Stop(true, ParticleSystemStopBehavior.StopEmitting);
            main = ps.main;
            main.loop = false;
            main.stopAction = ParticleSystemStopAction.Destroy;
        }
        if(destroyTime > 0) Destroy(gameObject, destroyTime);
    }

    void FixedUpdate()
    {
        if (ps && !ps.IsAlive()) Destroy(gameObject);
    }

    void StopEmitting()
    {
        if (ps && ps.IsAlive()) ps.Stop();
    }
}