﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public static class Utils
{
	[System.Serializable]
	public class IntEvent : UnityEvent<int> {}

	[System.Serializable]
	public class StringEvent : UnityEvent<string> {}

	public static GameObject InstantiateFollower(GameObject prefab, Transform target){
		GameObject follower = GameObject.Instantiate(prefab, target.position, target.rotation);
		if(follower.TryGetComponent(out Follower f)) f.target = target;
		return follower;
	}

	public static void Destroy(GameObject go){
		foreach(ParticleDestroyer pd in go.GetComponentsInChildren<ParticleDestroyer>()){
			pd.transform.SetParent(null);
			pd.Init();
		}
		GameObject.Destroy(go);
	}

	public static bool KillChildren(Transform transform){
		if(transform.childCount == 0) return false;
		foreach(Transform child in transform) Utils.Destroy(child.gameObject);
		return true;
	}

	public static float Angle( Vector2 a, Vector2 b ) {
		var an = a.normalized;
		var bn = b.normalized;
		var x = an.x * bn.x + an.y * bn.y;
		var y = an.y * bn.x - an.x * bn.y;
		return Mathf.Atan2(y, x) * Mathf.Rad2Deg;
	}

	public static Collider[] FindConeColliders(Vector3 pos, Vector3 dir, float angle, float distance, LayerMask mask){
		Collider[] colliders = Physics.OverlapSphere(pos, distance, mask);
		if(colliders.Length == 0) return colliders;
		return System.Array.FindAll(colliders, c => Vector3.Angle(dir, c.transform.position - pos) < angle);
	}

	public static Transform FindClosestTarget(Vector3 position, Collider[] targets, List<GameObject> ignore){
		ignore = new List<GameObject>(ignore);
		float lowestDistance = float.MaxValue;
		if(targets.Length == 0) return null;
		Collider closestTarget = targets[0];
		foreach(Collider target in targets){
			if(ignore.Contains(target.gameObject)) continue;
			if((target.transform.position - position).sqrMagnitude < lowestDistance){
				lowestDistance = (target.transform.position - position).sqrMagnitude;
				closestTarget = target;
			}
		}
		if(ignore.Contains(closestTarget.gameObject)) return null;
		return closestTarget.transform;
	}

	public static Transform FindClosestVisibleTarget(Vector3 position, Collider[] targets, List<GameObject> ignore, LayerMask mask){
		if(targets.Length == 0) return null;
		ignore = new List<GameObject>(ignore);
		
		foreach(Collider target in targets){
			if (!IsVisible(position, target.gameObject, mask)) ignore.Add(target.gameObject);
		}

		return FindClosestTarget(position, targets, ignore);
	}

	public static bool IsVisible(Vector3 from, GameObject target, LayerMask mask, Vector3? offset = null){
		if(offset == null) offset = Vector3.up;
		RaycastHit hitInfo;
		Physics.Linecast(from, target.transform.position + (Vector3)offset, out hitInfo, mask, QueryTriggerInteraction.Ignore);
		if(hitInfo.transform == target.transform) Debug.DrawLine(from, target.transform.position, Color.green);
		else Debug.DrawLine(from, target.transform.position, Color.red);
		return hitInfo.transform == target.transform;
	}
	
	public static Collider[] GetCollidersInRange(Vector3 position, float radius, LayerMask targetMask){
		return Physics.OverlapSphere(position, radius, targetMask);
	}

	public static string GetArg(string name) {
		var args = System.Environment.GetCommandLineArgs();
		for (int i = 0; i < args.Length; i++)
		{
			if (args[i] == name && args.Length > i + 1)
			{
				return args[i + 1];
			}
		}
		return null;
	}

	public static void Shuffle<T>(this IList<T> ts) {
		var count = ts.Count;
		var last = count - 1;
		for (var i = 0; i < last; ++i) {
			var r = UnityEngine.Random.Range(i, count);
			var tmp = ts[i];
			ts[i] = ts[r];
			ts[r] = tmp;
		}
	}

	public static void ApplyColor(Renderer[] renderers, Color color){
		foreach(Renderer renderer in renderers) foreach(Material mat in renderer.materials) mat.color = color;
	}

	public static bool PolygonContainsPoint(Vector2[] polyPoints, Vector2 p){
		var j = polyPoints.Length - 1;
		var inside = false;
		for (int i = 0; i < polyPoints.Length; j = i++)
		{
			var pi = polyPoints[i];
			var pj = polyPoints[j];
			if (((pi.y <= p.y && p.y < pj.y) || (pj.y <= p.y && p.y < pi.y)) &&
				(p.x < (pj.x - pi.x) * (p.y - pi.y) / (pj.y - pi.y) + pi.x))
				inside = !inside;
		}
		return inside;
	}

	public static bool IsPointInPolygon(Vector2 point, Vector2[] polygon) {
		int j = polygon.Length-1;
		bool c = false;
		for(int i=0;i<polygon.Length;j=i++)c^=polygon[i].y>point.y^polygon[j].y>point.y&&point.x<(polygon[j].x-polygon[i].x)*(point.y-polygon[i].y)/(polygon[j].y-polygon[i].y)+polygon[i].x;
		return c;
	}


}


/* example usage */
/*
	[SerializeField, GetSet("fillAmount")]
	private float _fillAmount = 0.5f;
	public float fillAmount {
		get {
			return _fillAmount;
		}
		set {
			Debug.Log("Calling set!");
			_fillAmount = value;
			fillImage.fillAmount = value;
		}
	}
*/
public sealed class GetSetAttribute : PropertyAttribute {
	public readonly string name;
	public bool dirty;
 
	public GetSetAttribute(string name) {
		this.name = name;
	}
}
