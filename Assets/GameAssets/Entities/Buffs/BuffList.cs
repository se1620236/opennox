﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffList : ScriptableObject {
	public List<BaseBuff> buffs = new List<BaseBuff>();

	public BaseBuff this[int i] {
		get { return buffs[i]; }
		set { buffs[i] = value; }
	}

	public int Count {
		get { return buffs.Count; }
	}

	public BaseBuff GetInstance(int i){
		return ScriptableObject.Instantiate(this[i]);
	}
}
