﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoneBuff : MonoBehaviour
{
    public BaseBuff buff;

	void OnCollisionEnter(Collision collision){
		ApplyBuff(collision.gameObject);
    }

    void OnTriggerEnter(Collider collider){
    	ApplyBuff(collider.gameObject);
    }

    void ApplyBuff(GameObject go){
    	if(go.TryGetComponent(out BuffSystem bs)){
    		bs.AddBuff(buff);
    	}
    }
}
