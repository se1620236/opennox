﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Buffs/Warrior Charge Buff")]
public class WarriorChargeBuff : BaseBuff {
	public BuffSkill sourceSkill;
	public float force;
	public float stopAngle = 65;
	public Damage enemyDamage;
	public Damage playerDamage;
	public BaseBuff stunBuff;

	Vector3 _vel;
	BaseSkill _sourceSkill;

	public override void Added(){
		base.Added();
		_sourceSkill = owner.skills.GetSkillOfType(sourceSkill);
		if(isServer){			
			_vel = owner.transform.forward * force;
			(owner as PlayerEntityScript).ignorePlayerInput = true;
			(owner as PlayerEntityScript).animation.SetSpeed(254);
			(owner as BaseMovingEntity).globalVelocities.Add(_vel);
			(owner as BaseMovingEntity).rotationMultipliers.Add(0);
			(owner as BaseMovingEntity).OnMovementHit.AddListener(Hit);
		}
	}

	public override void UpdateBuff(float deltaTime){
		(owner as BaseMovingEntity).direction = owner.transform.forward;
		base.UpdateBuff(deltaTime);
		_sourceSkill.cooldownTimer = _sourceSkill.cooldown;
		if(isServer && !(owner as BaseMovingEntity).isGrounded) Expired();
	}

	public virtual void Hit(ControllerColliderHit hit){
		if(Vector3.Angle(Vector3.up, hit.normal) < 65) return;
		if(hit.gameObject.GetComponent<Health>()){
			AttackSystem.ApplyDamage(hit.gameObject, enemyDamage);
			_sourceSkill.cooldownTimer = 0.5f;
		} else {
			owner.buffs.AddBuff(stunBuff);
			AttackSystem.ApplyDamage(owner.gameObject, playerDamage);
			_sourceSkill.cooldownTimer = _sourceSkill.cooldown;
		}		
		
		Expired();
	}

	public override void Expired(){
		base.Expired();
		if(isServer){
			(owner as PlayerEntityScript).ignorePlayerInput = false;
			(owner as BaseMovingEntity).globalVelocities.Remove(_vel);
			(owner as BaseMovingEntity).rotationMultipliers.Remove(0);
			(owner as BaseMovingEntity).OnMovementHit.RemoveListener(Hit);
		}
	}

}
