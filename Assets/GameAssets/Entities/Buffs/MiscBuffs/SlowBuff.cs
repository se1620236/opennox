﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Buffs/Slow Buff")]
public class SlowBuff : BaseBuff
{
	public float speedMultiplier;
	public float rotationMultiplier;

	public override void Added(){
		base.Added();
		if(isServer){
			(owner as BaseMovingEntity).movementMultipliers.Add(speedMultiplier);
			(owner as BaseMovingEntity).rotationMultipliers.Add(rotationMultiplier);
		}
	}

	public override void Expired(){
		base.Expired();
		if(isServer){
			(owner as BaseMovingEntity).movementMultipliers.Remove(speedMultiplier);
			(owner as BaseMovingEntity).rotationMultipliers.Remove(rotationMultiplier);
		}
	}
}
