﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAPI;
using MLAPI.Serialization;
using MLAPI.Serialization.Pooled;
using MLAPI.NetworkedVar;
using MLAPI.NetworkedVar.Collections;
using MLAPI.Messaging;

public class BuffSystem : NetworkedBehaviour
{
    public NetworkedList<int> buffsSync = new NetworkedList<int>(new MLAPI.NetworkedVar.NetworkedVarSettings()
    {
        ReadPermission = MLAPI.NetworkedVar.NetworkedVarPermission.Everyone,
        WritePermission = MLAPI.NetworkedVar.NetworkedVarPermission.ServerOnly
    }, new List<int>());

    public List<BaseBuff> buffs = new List<BaseBuff>();

    void Start(){
        buffsSync.OnListChanged += OnSyncListChanged;
    }

    void OnSyncListChanged(NetworkedListEvent<int> e){
    	if(IsServer) return;
    	switch (e.eventType){
            case NetworkedListEvent<int>.EventType.Add:
            	AddBuffClient(e.value, e.index);
            break;
            case NetworkedListEvent<int>.EventType.Insert:
            	AddBuffClient(e.value, e.index);
            break;
            case NetworkedListEvent<int>.EventType.Remove:
            	buffs[e.index].Expired();
            break;
            case NetworkedListEvent<int>.EventType.RemoveAt:
            	buffs[e.index].Expired();
            break;
            case NetworkedListEvent<int>.EventType.Value:
            	if(e.value == -1) {
            		buffs[e.index].Expired();
        		} else {
        			AddBuffClient(e.value, e.index);
        		}
            break;
            case NetworkedListEvent<int>.EventType.Clear:
            	foreach(BaseBuff buff in buffs) buff.Expired();
            break;
        }
    }

    public void AddBuff(BaseBuff buff, int index = -1){
    	AddBuff(buff.typeId, index);
    }

    public void AddBuff(int type, int index = -1){
    	if(!IsServer) return;
		BaseBuff buff = GameManager.buffList.GetInstance(type);
		
		if(!buff.stackable) {
			BaseBuff existingBuff = FindBuffOfType(type);
			if(existingBuff != null) {
				existingBuff.duration = buff.duration;
				return;
			}
		}

		if(index == -1) index = FindSlot();

		buff.OnExpired.AddListener(()=>{
			buffsSync[index] = -1;
		});

		buff.OnExpired.AddListener(()=>{
			buffs[index] = null;
		});

    	buff.isServer = IsServer;
    	buff.isClient = IsClient;
    	buff.isLocalPlayer = IsLocalPlayer;
        buff.owner = GetComponent<BaseNetworkedEntity>(); //TBD

		if(index >= buffsSync.Count) {
			buffsSync.Insert(index, type);
			buffs.Insert(index, buff);
		} else {
			buffsSync[index] = type;
			buffs[index] = buff;
		}

        buff.Added();

		Debug.Log("Insert buff SERVER "+buff.name+" at "+index);
    }

    void AddBuffClient(int type, int index){
    	BaseBuff buff = GameManager.buffList.GetInstance(type);
    	buff.isServer = IsServer;
    	buff.isClient = IsClient;
    	buff.isLocalPlayer = IsLocalPlayer;
        buff.owner = GetComponent<BaseNetworkedEntity>(); //TBD
        
		buff.OnExpired.AddListener(()=>{ buffs[index] = null; });		
		if(index >= buffs.Count) {
			buffs.Insert(index, buff);
		} else {
			buffs[index] = buff;
		}

        buff.Added();
		Debug.Log("Insert buff CLIENT "+buff.name+" at "+index);
    }

    int FindSlot(){
    	for(int i=0; i<buffsSync.Count; i++){
    		if(buffsSync[i] == -1) return i;
    	}
    	return buffsSync.Count;
    }

    BaseBuff FindBuffOfType(BaseBuff buff){
    	return FindBuffOfType(buff.typeId);
    }

    BaseBuff FindBuffOfType(int type){
    	for(int i=0; i<buffsSync.Count; i++){
    		if(buffsSync[i] == type) return buffs[i];
    	}
    	return null;
    }

    void LateUpdate() {
    	float deltaTime = Time.deltaTime;
        foreach(BaseBuff buff in new List<BaseBuff>(buffs)) {
        	if(buff != null) buff.UpdateBuff(deltaTime);
        }
    }
}
