﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Skills/Buff Skill")]
public class BuffSkill : BaseSkill
{
	public enum Target {Self, Other};

	public BaseBuff buff;
	public bool targetsSelf = true;
	public bool waitForBuffToEnd;
	public GameObject projectilePrefab;

	public override void Use(){
		base.Use();
		if(targetsSelf){
			owner.buffs.AddBuff(buff);
		} else {

		}
	}

	public override void Update(){
		base.Update();
	}
}
