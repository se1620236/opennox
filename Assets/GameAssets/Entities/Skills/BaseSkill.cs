﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BaseSkill : ScriptableObject
{
	[HideInInspector] public BaseNetworkedEntity owner;
	public new string name;
	public string description;
	public Sprite icon;
	public bool unlocked = false;
	public float cooldown = 1;
	[HideInInspector] public float cooldownTimer = 0f;

	public float coolDownNormal { 
		get {
			return cooldownTimer/cooldown;
		}
	}

	public bool usable { 
		get {
			return unlocked && cooldownTimer == 0f;
		}
	}

	public virtual void Use(){
		cooldownTimer = cooldown;
	}

	public virtual void Update(){
		if(cooldownTimer > 0) cooldownTimer = Mathf.MoveTowards(cooldownTimer, 0, Time.deltaTime);
	}
}
