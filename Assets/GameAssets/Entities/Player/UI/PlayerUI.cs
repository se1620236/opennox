﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
	public static PlayerEntityScript target;

	public GameObject gameUIContainer;
	public Image healthBar;
	public Image manaBar;
    public Image staminaBar;
    public Image[] skillIcons;
    public Image[] skillCooldowns;

    void Start() {
    }

    void Update() {
        gameUIContainer.SetActive(target != null);
        if(target) UpdateGameUI();
    }

    void UpdateGameUI(){
    	healthBar.fillAmount = (float)target.health.amount/(float)target.health.maxAmount;
    	manaBar.fillAmount = (float)target.mana.amount/(float)target.mana.maxAmount;
    	staminaBar.fillAmount = target.attacks.coolDown/1f; //TODO

        for(int i=0; i < skillIcons.Length; i++){
            BaseSkill skill = target.playerSkills.GetSkillAtSlot(i);
            if(skill == null) continue;
            skillIcons[i].sprite = skill.icon;
            skillCooldowns[i].fillAmount = skill.coolDownNormal;
        }
    }
}
