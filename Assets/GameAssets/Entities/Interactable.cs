﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
	public enum Type {Attack, Interact, Pickup};
	public Type type;
    public virtual void OnInteracted(){}
}
