﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Damage {
	public enum Type {
		Blade, 
		Crush, 
		Impale, 
		Poison, 
		Explosion, 
		Drain,
		Flame, 
		Electrical,
		//DispelUndead, //Commented until needed 
		Death,
		Mana
	};
    [HideInInspector] public BaseNetworkedEntity source;
    public bool ignoreSource = true;
	public int amount;
}
