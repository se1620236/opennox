﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Attack {
    [HideInInspector] public BaseNetworkedEntity source;
    public Damage damage;
    public float coolDown;
    public float attackDelay;
    public float stopMovementTime;
    public GameObject prefab;
}
