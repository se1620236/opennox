﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class BaseMovingEntity : BaseNetworkedEntity
{
    public List<float> rotationMultipliers = new List<float>();
    public List<float> movementMultipliers = new List<float>();
    public List<Vector3> globalVelocities = new List<Vector3>();
    public List<Vector3> localVelocities = new List<Vector3>();
    public List<Impulse> impulses = new List<Impulse>();
	public float movementSpeed;
    public float rotationSpeed = 1000;
    public float targetRotation = 0;
	public Vector3 direction;
    public float gravity = -9.81f;
    public bool isGrounded = false;

    [System.Serializable]
    public class MovementHitEvent : UnityEvent<ControllerColliderHit>{}

    public MovementHitEvent OnMovementHit;

    float _gravity;


    [System.Serializable]
    public class Impulse {
        public bool local = false;
        public AnimationCurve xCurve;
        public AnimationCurve yCurve;
        public AnimationCurve zCurve;
        public float duration = 1f;

        [HideInInspector] public Quaternion rotation;
        [HideInInspector] public Vector3 lastVector;
        [HideInInspector] public float time;        
    }

    protected override void ServerUpdate(){
    	base.ServerUpdate();
        if(isGrounded) _gravity = gravity * Time.deltaTime;
        else _gravity += gravity * Time.deltaTime;
    	
        Vector3 input = direction * movementSpeed;
        foreach(float multiplier in movementMultipliers) input *= multiplier;
    	if(
            !isGrounded ||
            (attacks && attacks.stopMovementTime > 0)
        ) input *= 0;
        input += Vector3.up * _gravity;
        foreach(Vector3 velocity in globalVelocities) input += velocity;
        foreach(Vector3 velocity in localVelocities) input += transform.TransformDirection(velocity);
    	controller.Move(input * Time.deltaTime);

        isGrounded = (controller.collisionFlags & CollisionFlags.Below) != 0;
        if(animation) animation.SetGrounded(isGrounded);

        Quaternion rot = Quaternion.Euler(0,targetRotation,0);
        float rs = rotationSpeed;
        foreach(float multiplier in rotationMultipliers) rs *= multiplier;
        transform.rotation = Quaternion.RotateTowards(transform.rotation, rot, rs * Time.deltaTime);
    }

    protected override void ServerFixedUpdate(){
        base.ServerFixedUpdate();
        foreach(Impulse impulse in new List<Impulse>(impulses)){
            impulse.time += Time.fixedDeltaTime;
            if(impulse.local) localVelocities.Remove(impulse.lastVector);
            else globalVelocities.Remove(impulse.lastVector);

            if(impulse.time > impulse.duration){
                impulses.Remove(impulse);
                continue;
            }

            Vector3 vector = new Vector3(
                impulse.xCurve.Evaluate(impulse.time),
                impulse.yCurve.Evaluate(impulse.time),
                impulse.zCurve.Evaluate(impulse.time)
            );
            if(impulse.local) localVelocities.Add(vector);
            else {
                vector = impulse.rotation * vector;
                globalVelocities.Add(vector);
            }
            impulse.lastVector = vector;
        }
    }

    public Impulse AddImpulse(Impulse impulse, Vector3 direction){
        Impulse clone = new Impulse();
        clone.local = impulse.local;
        clone.duration = impulse.duration;
        clone.xCurve = impulse.xCurve;
        clone.yCurve = impulse.yCurve;
        clone.zCurve = impulse.zCurve;

        Vector3 vector = new Vector3(
            clone.xCurve.Evaluate(clone.time),
            clone.yCurve.Evaluate(clone.time),
            clone.zCurve.Evaluate(clone.time)
        );
        if(direction != null) clone.rotation = Quaternion.LookRotation(direction);
        if(clone.local){
            localVelocities.Remove(clone.lastVector);
            localVelocities.Add(vector);
        } else {
            vector = clone.rotation * vector;
            globalVelocities.Remove(clone.lastVector);
            globalVelocities.Add(vector);
        }
        clone.lastVector = vector;

        impulses.Add(clone);

        return clone;
    }

    void OnControllerColliderHit(ControllerColliderHit hit) {
        OnMovementHit.Invoke(hit);
    }
}
